#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <string>
#include <glm/glm.hpp>

#include "noise.h"


namespace bg {
	// https://github.com/wwwtyro/space-2d/blob/gh-pages/nebula.js#L52
 
	struct Star {
		glm::vec2 pos;
		float coreRadius;
		float haloFallOff;
		float scale;
		glm::vec3 coreColor;
		glm::vec3 haloColor;
	};

	struct MinMax {
		std::size_t min;
		std::size_t max;
		MinMax(std::size_t min, std::size_t max) : min(min), max(max) {  } 
	};

	class MemoryTexture {
		public:
			enum Color {
				r = 0,
				g = 1,
				b = 2,
				a = 3
			};

		private:
			uint8_t* data;

			std::size_t w;
			std::size_t h;

			static const int ChannelCount = 4;

			void writeTo(std::size_t pixelNumber, Color color, uint8_t value);
			uint8_t* dataAt(std::size_t x, std::size_t y, Color c);
			void writeTo(std::size_t x, std::size_t y, Color color, uint8_t value);
		
		protected:
			MemoryTexture(std::size_t width, std::size_t height);

			void fill(glm::vec4 const& color);

			std::size_t width() const;
			std::size_t height() const;

			glm::vec4 getColor(std::size_t x, std::size_t y);
			void setColor(std::size_t x, std::size_t y, glm::vec4 const& color);
			void setColor(std::size_t x, std::size_t y, uint8_t r, uint8_t g, uint8_t b, uint8_t a);
			void mixColor(std::size_t x, std::size_t y, glm::vec4 mixY, glm::vec4 const& mixA);
			void addColor(std::size_t x, std::size_t y, glm::vec4 const& color);

		public:
			void exportPngRgba(std::string const& name);
			
			virtual ~MemoryTexture();
	};

	class SpaceTextureGen : public MemoryTexture {
		protected:
			void generatePointStars(float density, float brightness, MinMax width, MinMax height);
			void generateNebulae(glm::vec3 const& color, glm::vec2 const& offset, float scale, float density, float falloff, MinMax width, MinMax height);

			void addStars(std::vector<Star> const& starsVector, MinMax width, MinMax height);

			float noise(glm::vec2 const& pos, glm::vec2 const& offset, noise::Perlin & noise);
		
		public:
			explicit SpaceTextureGen(std::size_t width, std::size_t height);
			SpaceTextureGen() = delete;
	};

	class ThreadedSTG final : public SpaceTextureGen {
		public:
			ThreadedSTG(std::size_t width, std::size_t height, std::size_t concurrency);
			~ThreadedSTG();
	};

	class CubeMap {
		public:
			CubeMap(std::size_t width, std::size_t height, std::string const& prefix);
	};
}







#endif // BACKGROUND_H