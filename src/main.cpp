#include <iostream>

// ici il y a la classe qui gere tout bg::SpaceTextureGen
#include "background.h"

int main(int argc, char* argv[]) {
    //std::cout << "Generation" << std::endl;

    // genere une texture dans le dossier courant, de taille 512px x 512px
    // bg::SpaceTextureGen spGen(3840, 1080);

    // bg::ThreadedSTG spGen(3840, 1080, 1);
    // std::cout << std::endl;
    bg::ThreadedSTG spGen2(4096, 4096, 6);

    //std::cout << "Fin" << std::endl;
    return EXIT_SUCCESS;
}
