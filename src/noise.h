#ifndef NOISE_H
#define NOISE_H

#include <vector>

#include <glm/glm.hpp>

// https://gpfault.net/posts/perlin-noise.txt.html

namespace noise {
    // https://github.com/sol-prog/Perlin_Noise
    class Perlin {
        private:
            std::vector<int> permutationVector;

            float fade(float t);
            float lerp(float t, float a, float b);
            float grad(int hash, float x, float y, float z);


        public:
            Perlin();
            Perlin(unsigned seed);

            float noise(float x, float y, float z);
            float normalNoise(glm::vec2 const& pos);
    };








}

#endif // NOISE_H