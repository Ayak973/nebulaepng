#include "background.h"


#include <random>
#include <stdexcept>
#include <cmath>
#include <thread>
#include <vector>

#include <iostream>

#include <chrono>


#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"


//////////////MEMORYTEXTURE

bg::MemoryTexture::MemoryTexture(std::size_t width, std::size_t height) : w(width), h(height) {
    if (h < 2 || w < 2) throw std::runtime_error("invalid parameters");
    try {
        data = new uint8_t[w * h * ChannelCount]();
    }
    catch(...) {
        std::cout << "Error: cannot allocate buffer for data !!" << std::endl;
        std::terminate();
    }
}

bg::MemoryTexture::~MemoryTexture() {
    delete[] data;
}

void bg::MemoryTexture::writeTo(std::size_t x, std::size_t y, Color color, uint8_t value) {
    data[y * w * 4 + x * 4 + color] = value;
}

void bg::MemoryTexture::writeTo(std::size_t pixelNumber, Color color, uint8_t value) {
    data[pixelNumber * 4 + color] = value;
}

void bg::MemoryTexture::fill(glm::vec4 const& color) {
    std::size_t size = w * h;
    for(auto idx = 0u; idx < size; ++idx) {
        writeTo(idx, Color::r, color.r * 255.f);
        writeTo(idx, Color::g, color.g * 255.f);
        writeTo(idx, Color::b, color.b * 255.f);
        writeTo(idx, Color::a, color.a * 255.f);
    }
}

uint8_t* bg::MemoryTexture::dataAt(std::size_t x, std::size_t y, Color c) {
    return &data[y * w * 4 + x * 4 + c];
}

void bg::MemoryTexture::exportPngRgba(std::string const& name) {
    stbi_write_png(name.c_str(), w, h, ChannelCount, data, w * ChannelCount);
}

std::size_t bg::MemoryTexture::width() const {
    return w;
}

std::size_t bg::MemoryTexture::height() const {
    return h;
}

glm::vec4 bg::MemoryTexture::getColor(std::size_t x, std::size_t y) {
    return glm::vec4((*dataAt(x, y, Color::r)) / 255.f, (*dataAt(x, y, Color::g)) / 255.f, (*dataAt(x, y, Color::b)) / 255.f, (*dataAt(x, y, Color::a)) / 255.f);
}
 
void bg::MemoryTexture::setColor(std::size_t x, std::size_t y, glm::vec4 const& color) {
    writeTo(x, y, Color::r, std::min(1.f, color.r) * 255.f);
    writeTo(x, y, Color::g, std::min(1.f, color.g) * 255.f);
    writeTo(x, y, Color::b, std::min(1.f, color.b) * 255.f);
    writeTo(x, y, Color::a, std::min(1.f, color.a) * 255.f);
}

void bg::MemoryTexture::setColor(std::size_t x, std::size_t y, uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
    writeTo(x, y, Color::r, r);
    writeTo(x, y, Color::g, g);
    writeTo(x, y, Color::b, b);
    writeTo(x, y, Color::a, a);
}

void bg::MemoryTexture::mixColor(std::size_t x, std::size_t y, glm::vec4 mixY, glm::vec4 const& mixA) {
    glm::vec4 mix = glm::max(glm::min(glm::mix(getColor(x, y), mixY, mixA), glm::vec4(1.f)), glm::vec4(0.f));
    writeTo(x, y, Color::r, mix.r * 255.f);
    writeTo(x, y, Color::g, mix.g * 255.f);
    writeTo(x, y, Color::b, mix.b * 255.f);
    writeTo(x, y, Color::a, mix.a * 255.f);
}

void bg::MemoryTexture::addColor(std::size_t x, std::size_t y, glm::vec4 const& color) {
    glm::vec4 dest = glm::max(glm::min(color + getColor(x, y), glm::vec4(1.f)), glm::vec4(0.f));
    writeTo(x, y, Color::r, dest.r * 255.f);
    writeTo(x, y, Color::g, dest.g * 255.f);
    writeTo(x, y, Color::b, dest.b * 255.f);
    writeTo(x, y, Color::a, dest.a * 255.f);
}


//////////////SPACETEXTUREGEN

bg::SpaceTextureGen::SpaceTextureGen(std::size_t w, std::size_t h) : MemoryTexture(w, h) { }

void bg::SpaceTextureGen::generatePointStars(float density, float brightness, MinMax width, MinMax height) {
    std::random_device                      rd{};
    std::mt19937                            mt{rd()};
    std::uniform_int_distribution<std::size_t>  distW(width.min, width.max - 1);
    std::uniform_int_distribution<std::size_t>  distH(height.min, height.max - 1);
    std::exponential_distribution<float> distExp(brightness);

    std::size_t count = std::round(this->width() * this->height() * density);
    for(std::size_t idx = 0; idx < count; ++idx) {
        std::size_t x = distW(mt);
        std::size_t y = distH(mt);
        uint8_t randValue = distExp(mt);
        setColor(x, y, randValue, randValue, randValue, 255);
    }
}

float bg::SpaceTextureGen::noise(glm::vec2 const& pos, glm::vec2 const& offset, noise::Perlin & noise) {
    glm::vec2 p = pos + offset;
    const auto steps = 5u;
    float scale = std::pow(2.f, steps);
    float displace = 0.f;
    for (auto idx = 0u; idx < steps; ++idx) {
        displace = noise.normalNoise(p * scale + displace);
        scale *= 0.5;
    }
    return noise.normalNoise(p + displace);
}

void bg::SpaceTextureGen::generateNebulae(glm::vec3 const& color, glm::vec2 const& offset, float scale, float density, float falloff, MinMax width, MinMax height) {
    noise::Perlin perlin;
    float ratio = (float) this->width() / (float) this->height();
    float n = 0.f;
    for(std::size_t x = width.min; x < width.max; ++x) {
        for (std::size_t y = height.min; y < height.max; ++y) {
            glm::vec2 pos = glm::vec2(x / (float) this->width() * ratio, y / (float) this->height());
            n = std::min(1.f, std::pow(noise(pos * scale * 1.f, offset, perlin) + density, falloff));
            mixColor(x, y, glm::vec4(color, 1.f), glm::vec4(n, n, n, n));
        }
    }
}

void bg::SpaceTextureGen::addStars(std::vector<Star> const& starsVector, MinMax width, MinMax height) {
    if (starsVector.size() == 0) return;
    float e = 0.f;
    glm::vec3 rgb(0.f);
    float ratio = (float) this->width() / (float) this->height();
    for (std::size_t x = width.min; x < width.max; ++x) {
        for(std::size_t y = height.min; y < height.max; ++y) {
            for (auto & star : starsVector) {
                float length = glm::distance(glm::vec2(x / (float)this->width() * ratio, y / (float)this->height()), star.pos) / star.scale;
                if (length <= ((star.coreRadius))) {
                    setColor(x, y, glm::vec4(star.coreColor, 1.f));
                    continue;
                }
                e = 1.f - std::exp(-(length - star.coreRadius) * star.haloFallOff);
                rgb = glm::mix(star.coreColor, star.haloColor, e);
                rgb = glm::mix(rgb, glm::vec3(0.f), e);
                addColor(x, y, glm::vec4(rgb, 1.f));
            }
        }
    }
}



//////////////THREADEDSTG

bg::ThreadedSTG::ThreadedSTG(std::size_t width, std::size_t height, std::size_t concurrency) : SpaceTextureGen(width, height) {
    if (concurrency == 0) throw std::runtime_error("concurrency should be at least equal to 1");
    if (concurrency > width || concurrency > height) throw std::runtime_error("concurrency should not be > width or > height");
    std::cout << "Generation parameters " << width << "x" << height << " - concurrency: " << concurrency << " threads" << std::endl; 

    auto startTotal = std::chrono::high_resolution_clock::now();
    auto start = std::chrono::high_resolution_clock::now();
    fill(glm::vec4(0.f, 0.f, 0.f, 1.f));
    auto fillTime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count();
    std::cout << "Fill time: \t\t" << (fillTime/ 1000.f) << "ms" << std::endl;

    start = std::chrono::high_resolution_clock::now();
    generatePointStars(0.125f, 0.01f, {0, width - 1}, {0, height});
    auto pointStarTime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count();
    std::cout << "Point star time:  \t" << (pointStarTime/ 1000.f) << "ms" << std::endl;

    start = std::chrono::high_resolution_clock::now();

    if (concurrency > 1) {
        std::vector<std::thread> threadVector;
        std::size_t doneWidth = 0, chunckWidth = width / concurrency, lastWidth = 0, maxWidth = 0;
        for (auto idx = 0u; idx < concurrency; ++idx) {
            maxWidth = idx * chunckWidth + chunckWidth;
            if (idx == (concurrency - 1)) {
                maxWidth = width;
            }
            threadVector.emplace_back(std::thread(&ThreadedSTG::generateNebulae, this, glm::vec3(0.0f, 0.1f, 0.4f), glm::vec2(0.f,0.f), 2.5f, 0.2f, 2.5f, MinMax(lastWidth, maxWidth), MinMax(0, height)));
            doneWidth += chunckWidth;
            lastWidth = idx * chunckWidth + chunckWidth;
        }
        for (auto& it : threadVector) if(it.joinable()) it.join();
    }
    else generateNebulae(glm::vec3(0.0f, 0.1f, 0.4f), glm::vec2(0.f,0.f), 2.5f, 0.2f, 2.5f, MinMax(0, width), MinMax(0, height));

    auto nebulae1Time = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count();
    std::cout << "Gen nebulae 1:  \t" << (nebulae1Time/ 1000.f) << "ms" << std::endl;

    start = std::chrono::high_resolution_clock::now();
        if (concurrency > 1) {
        std::vector<std::thread> threadVector;
        std::size_t doneWidth = 0, chunckWidth = width / concurrency, lastWidth = 0, maxWidth = 0;
        for (auto idx = 0u; idx < concurrency; ++idx) {
            maxWidth = idx * chunckWidth + chunckWidth;
            if (idx == (concurrency - 1)) {
                maxWidth = width;
            }
            threadVector.emplace_back(std::thread(&ThreadedSTG::generateNebulae, this, glm::vec3(1.0f, 0.1f, 0.1f), glm::vec2(0.f,0.f), 2.0f, 0.05f, 10.f, MinMax(lastWidth, maxWidth), MinMax(0, height)));
            doneWidth += chunckWidth;
            lastWidth = idx * chunckWidth + chunckWidth;
        }
        for (auto& it : threadVector) if(it.joinable()) it.join();
    }
    else generateNebulae(glm::vec3(1.0f, 0.0f, 0.0f), glm::vec2(0.f,0.f), 2.0f, 0.05f, 10.f, {0, width - 1}, { 0, height - 1});
    auto nebulae2Time = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count();
    std::cout << "Gen nebulae 2:  \t" << (nebulae2Time/ 1000.f) << "ms" << std::endl;

    start = std::chrono::high_resolution_clock::now();
    std::vector<Star> stars{
        { glm::vec2(0.75f,0.75f), 0.01f, 10.f, 0.05f, glm::vec3(0.99f,0.99f,0.99f), glm::vec3(0.25f,0.25f,0.8f) },
        { glm::vec2(0.62f,0.70f), 0.01f, 10.f, 0.05f, glm::vec3(0.99f,0.99f,0.99f), glm::vec3(0.5f,0.25f,0.1f) },
        { glm::vec2(0.33f,0.45f), 0.05f, 10.f, 1.f, glm::vec3(0.96f,0.96f,0.96f), glm::vec3(0.25f,0.25f,0.8f) },
    };
    if (concurrency > 1) {
        std::vector<std::thread> threadVector;
        std::size_t doneWidth = 0, chunckWidth = width / concurrency, lastWidth = 0, maxWidth = 0;
        for (auto idx = 0u; idx < concurrency; ++idx) {
            maxWidth = idx * chunckWidth + chunckWidth;
            if (idx == (concurrency - 1)) {
                maxWidth = width;
            }
            threadVector.emplace_back(std::thread(&ThreadedSTG::addStars, this, stars, MinMax(lastWidth, maxWidth), MinMax(0, height)));
            doneWidth += chunckWidth;
            lastWidth = idx * chunckWidth + chunckWidth;
        }
        for (auto& it : threadVector) if(it.joinable()) it.join();
    }
    else addStars(stars, { 0, width - 1}, { 0, height - 1});
    

    auto addStarTime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count();
    std::cout << "Add 3 stars:  \t\t" << (addStarTime/ 1000.f) << "ms" << std::endl;

    start = std::chrono::high_resolution_clock::now();
    exportPngRgba("output.png");
    auto writeTime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count();

    std::cout << "Write file:  \t\t" << (writeTime / 1000.f) << "ms" << std::endl;

    auto totalTime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - startTotal).count();
    auto cumulated = fillTime + pointStarTime + nebulae1Time + nebulae2Time + addStarTime + writeTime;

    std::cout << "Totals: " <<std::endl;
    std::cout << "\tReal:  \t\t" << (totalTime / 1000.f) << "ms" << std::endl;
    std::cout << "\tAccumulated:  \t" << (cumulated / 1000.f) << "ms" << std::endl;
}

bg::ThreadedSTG::~ThreadedSTG() {

}



//////////////CUBEMAP

bg::CubeMap::CubeMap(std::size_t width, std::size_t height, std::string const& path) {
    for (std::size_t idx = 0; idx < 6; ++idx) {
        
    }
}