# Output file name
DISTNAME           	= textgen

# Output directory
DISTDIR				= build

# Includes dir
INCDIRS            	= 

# Sources dir
SRCDIR				= src

# Define for old hardware (disable tesselation)
OLDHARDWARE = #-DOLD_HW

# Library dir and modules
LIBDIR             	= 
LIBS				= -lpthread

# Tools path
CXX                	= @g++
COPY_EXEC          	= @cp -rf
MOVE_EXEC          	= @mv -f
MKDIR_EXEC         	= mkdir -p
TEST_EXEC          	= @test -d
ECHO_EXEC          	= @echo
RM_EXEC            	= @rm -rf
DELETEOBJS			= find . -type f -name '*.o' -exec rm {} +

# C++ flags
CXXFLAGS           	= -c -Wall -std=c++17 -Werror

SOURCES           	= $(shell find $(SRCDIR)/ -name '*.cpp')

# Normal headers
HEADERS				= $(notdir $(shell find . -maxdepth 1 -name '*.h'))

# Objects files
OBJS				= $(SOURCES:%.cpp=%.o)


DEBUG_SANITIZE = -fsanitize=address -O1

release: CXXFLAGS     	+= -O3
release: releaseinfo createdir $(DISTNAME) cleanbuild
	$(ECHO_EXEC) Release done !

# Add DEBUG define and debug infos for gdb (-ggdb)
debug: CXXFLAGS += -D_DEBUG $(OLDHARDWARE) $(DEBUG_SANITIZE)
debug: CXXFLAGS += -ggdb 
debug: debuginfo createdir $(DISTNAME)
	$(ECHO_EXEC) Debug done !

# Executable rule
$(DISTNAME): $(OBJS)
	$(ECHO_EXEC) Building $(DISTNAME)
	$(CXX) $^ $(LIBS) $(LIBDIR) $(DEBUG_SANITIZE) -o $(DISTDIR)/$@

# Generate object files
$(OBJS): %.o : %.cpp
	$(CXX) $(CXXFLAGS) $(INCDIRS) -c $< -o $@

cleanbuild:
	@echo Cleaning obj dir
	@$(DELETEOBJS)

debuginfo:
	$(ECHO_EXEC) Starting Debug for $(DISTNAME)

releaseinfo:
	$(ECHO_EXEC) Starting Release for $(DISTNAME)

createdir:
	@$(MKDIR_EXEC) $(DISTDIR)

.PHONY: clean
clean: cleanbuild
	$(RM_EXEC) $(DISTDIR)
	$(ECHO_EXEC) Clean done !